$(document).ready(function() {
    $('form').on('submit', function(event) {
        event.preventDefault();
        // Effectuer la logique de traitement de formulaire ici
        $('#success-message').fadeIn().delay(3000).fadeOut();
    });
});