package com.ugb.msusers.controller;

import com.ugb.msusers.dao.UserDao;
import com.ugb.msusers.models.UserModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin(origins = "*")
public class UserController {

  @Autowired
  private UserDao userDao;

  @GetMapping(path = "/api/users/list")
  public ResponseEntity<List<UserModel>> list() {
    List<UserModel> users = userDao.findAll();
    return ResponseEntity.status(HttpStatus.OK).body(users);
  }

  @PostMapping(path = "/api/users/create")
  public ResponseEntity<UserModel> create(@RequestBody UserModel user) {
    if (userDao.existsByEmail(user.getEmail())) {
      throw new IllegalArgumentException("Cette utilisateur existe déjà");
    }
    UserModel newUser = userDao.save(user);
    return ResponseEntity.status(HttpStatus.CREATED).body(newUser);
  }
}
